package oneRain.UpMagazine;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
public class ShowActivity extends Activity
{
private int i = 1;
private int pos = 0;
private List<String> contents = null;
private static final String DIR = "/mnt/sdcard/UpMagazine/2010/content/";
 
//设置是否展开
private boolean isFolded = true;
 
//设置控件
private FrameLayout layout = null;
private Gallery showGallery = null;
private Button unfoldButton = null;
private TextView textView = null;
private TextView titleTextView = null;
 
public void onCreate(Bundle savedInstanceState)
{
super.onCreate(savedInstanceState);
requestWindowFeature(Window.FEATURE_NO_TITLE);
setContentView(R.layout.show);
 
initView();
}
 
@Override
protected void onResume() 
{
// TODO Auto-generated method stu
super.onResume();
 
isFolded = true;
}
//初始化
private void initView()
{ 
 
contents = new ArrayList<String>();
 
File dir = new File(DIR);
File[] files = dir.listFiles();
 
for(int i=0; i<files.length; i++)
{
contents.add(DIR + files<i>.getName());
}
 
layout = (FrameLayout)findViewById(R.id.layout);
 
unfoldButton = (Button)findViewById(R.id.unfoldButton);
unfoldButton.setOnClickListener(new UnfoldClickListener());
 
showGallery = (Gallery)findViewById(R.id.showGallery);
showGallery.setOnItemSelectedListener(new GalleryOnItemSelectedListener());
showGallery.setAdapter(new ShowAdapter());
 
titleTextView = (TextView)findViewById(R.id.titleTextView);
}
 
//滑动监听
private class GalleryOnItemSelectedListener implements OnItemSelectedListener
{
public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
long arg3) 
{
// TODO Auto-generated method stub
pos = arg2 + 1;
 
titleTextView.setText("第" + pos +"个主题");
}
public void onNothingSelected(AdapterView<?> arg0)
{
// TODO Auto-generated method stub
 
}
 
}
 
//按钮监听，展开一个透明的显示文本的遮挡层
private class UnfoldClickListener implements OnClickListener
{
public void onClick(View v) 
{ 
if(isFolded)
{
textView = new TextView(ShowActivity.this);
 
textView.setTextColor(Color.BLUE);
textView.setTextSize(20);
textView.setText("滚滚长江东逝水，浪花淘尽英雄。/n" +
"是非成败转头空，/n" +
"青山依旧在，几度夕阳红。/n" +
"白发渔樵江渚上，惯看秋月春风。 /n" +
"一壶浊酒喜相逢，/n" +
"古今多少事，都付笑谈中。");
textView.setGravity(Gravity.CENTER);
textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 
ViewGroup.LayoutParams.FILL_PARENT));
textView.setBackgroundColor(Color.parseColor("#86222222"));
 
unfoldButton.setText("收回");
 
isFolded = false;
 
layout.addView(textView);
}
else
{
unfoldButton.setText("展开");
 
isFolded = true;
 
layout.removeView(textView);
}
}
}
 
private class ShowAdapter extends BaseAdapter
{
 
public int getCount() 
{
// TODO Auto-generated method stub
return contents.size();
}
public Object getItem(int position) 
{
// TODO Auto-generated method stub
return position;
}
public long getItemId(int position) 
{
// TODO Auto-generated method stub
return 0;
}
public View getView(int position, View convertView, ViewGroup parent) 
{
// TODO Auto-generated method stub
ImageView i = new ImageView(ShowActivity.this);
 
Bitmap bm = BitmapFactory.decodeFile(contents.get(position));
// i.setLayoutParams(new Gallery.LayoutParams(Gallery.LayoutParams.FILL_PARENT,
// Gallery.LayoutParams.FILL_PARENT));
i.setScaleType(ImageView.ScaleType.FIT_XY);
i.setImageBitmap(bm);
 
return i;
}
}
}</i>